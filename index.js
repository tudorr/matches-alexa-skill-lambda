/* eslint-disable  func-names */
/* eslint-disable  no-console */

const Alexa = require('ask-sdk-core');
const fetch = require('node-fetch');

const getSlotValue = (requestEnvelope, key) => {
  if (
    !requestEnvelope
    || !requestEnvelope.request
    || !requestEnvelope.request.intent
    || !requestEnvelope.request.intent.slots
    || !requestEnvelope.request.intent.slots[key]
  ) {
    return null;
  }

  return requestEnvelope.request.intent.slots[key].value;
};

const LaunchRequestHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'LaunchRequest';
  },
  handle(handlerInput) {
    const speechText = 'Welcome to the Matches Fashion Skill';

    return handlerInput.responseBuilder
      .speak(speechText)
      .getResponse();
  },
};

const ShowIntentHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest'
      && handlerInput.requestEnvelope.request.intent.name === 'ShowIntent';
  },
  handle(handlerInput) {
    const gender = getSlotValue(handlerInput.requestEnvelope, 'gender');
    const category = getSlotValue(handlerInput.requestEnvelope, 'category');
    const speechText = `Showing ${gender}'s ${category}`;

    return fetch(
      'http://matches-alexa-skill.eu-west-2.elasticbeanstalk.com/api/changeUrl',
      {
        method: 'POST',
        body: JSON.stringify({ term: `${gender} ${category}` }),
        headers: { 'Content-Type': 'application/json' },
      }
    )
      .then(res => res.json())
      .then(json => {
        console.log(json);
        if (json.ok) {
          return handlerInput.responseBuilder
            .speak(speechText)
            .getResponse();
        }
        return handlerInput.responseBuilder
          .speak('Something went wrong')
          .getResponse();
      })
      .catch(() => {
        return handlerInput.responseBuilder
          .speak('Something went wrong')
          .getResponse();
      });
  },
};

const HelpIntentHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest'
      && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.HelpIntent';
  },
  handle(handlerInput) {
    const speechText = 'You can say hello to me!';

    return handlerInput.responseBuilder
      .speak(speechText)
      .reprompt(speechText)
      .withSimpleCard('Hello World', speechText)
      .getResponse();
  },
};

const CancelAndStopIntentHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest'
      && (handlerInput.requestEnvelope.request.intent.name === 'AMAZON.CancelIntent'
        || handlerInput.requestEnvelope.request.intent.name === 'AMAZON.StopIntent');
  },
  handle(handlerInput) {
    const speechText = 'Goodbye!';

    return handlerInput.responseBuilder
      .speak(speechText)
      .withSimpleCard('Hello World', speechText)
      .getResponse();
  },
};

const SessionEndedRequestHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'SessionEndedRequest';
  },
  handle(handlerInput) {
    console.log(`Session ended with reason: ${handlerInput.requestEnvelope.request.reason}`);

    return handlerInput.responseBuilder.getResponse();
  },
};

const ErrorHandler = {
  canHandle() {
    return true;
  },
  handle(handlerInput, error) {
    console.log(`Error handled: ${error.message}`);

    return handlerInput.responseBuilder
      .speak('Sorry, I can\'t understand the command. Please say again.')
      .reprompt('Sorry, I can\'t understand the command. Please say again.')
      .getResponse();
  },
};

const skillBuilder = Alexa.SkillBuilders.custom();

exports.handler = skillBuilder
  .addRequestHandlers(
    LaunchRequestHandler,
    ShowIntentHandler,
    HelpIntentHandler,
    CancelAndStopIntentHandler,
    SessionEndedRequestHandler,
  )
  .addErrorHandlers(ErrorHandler)
  .lambda();
